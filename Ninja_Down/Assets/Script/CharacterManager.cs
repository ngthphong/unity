﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterManager : MonoBehaviour
{
    private bool m_bStop;
    private bool m_bIsLeft;
    private float m_fCentral;
    private Animator m_a_Controller;              // Animator Controller
    private SpriteRenderer Sprite;
    //Sounds
    public AudioClip auSource;
    public AudioClip auSourceDie;
    public AudioClip auSourcex2;
    public AudioClip auSourceLifepoint;
    private float m_fSpeed;
    private float m_fDistant;
    public int ID;
    private bool isTouched;
    private float m_flocalScaleAspect;
    //private bool m_bIsDead;

    // Use this for initialization
    void Start()
    {
        Destroyer.m_bIsDead = false;
        m_flocalScaleAspect = 0.9f;
        m_fSpeed = 10f;
        m_bStop = true;
        isTouched = false;
        Sprite = GetComponent<SpriteRenderer>();
        m_a_Controller = GetComponent<Animator>();
        m_fCentral = Screen.width / 2.0f;
        switch (ID)
        {
            case 1:
                Sprite.transform.localScale = new Vector3((Screen.width * 1.0f / Screen.height), (Screen.width * 1.0f / Screen.height));
                m_bIsLeft = true;
                break;
            case 2:
                Sprite.transform.localScale = new Vector3(-(Screen.width * 1.0f / Screen.height), (Screen.width * 1.0f / Screen.height));
                m_bIsLeft = false;
                break;
            default:
                break;
        }
        switch (ID)
        {
            case 1:
                m_fDistant = GameObject.Find("LeftTree").transform.position.x - Sprite.transform.localScale.x;
                Sprite.transform.position = new Vector3(GameObject.Find("LeftTree").transform.position.x + Sprite.transform.localScale.x * 0.85f, (Screen.width * 1.0f / Screen.height) * 5.0f, -0.1f);
                m_a_Controller.Play("idle1");
                transform.gameObject.SetActive(true);
                break;
            case 2:
                m_fDistant = GameObject.Find("RightTree").transform.position.x - Sprite.transform.localScale.x;
                Sprite.transform.position = new Vector3(GameObject.Find("RightTree").transform.position.x + Sprite.transform.localScale.x * 0.85f, (Screen.width * 1.0f / Screen.height) * 5.0f, -0.1f);
                m_a_Controller.Play("idle");
                transform.gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }

    void Update()
    {
        Touch[] myTouches = Input.touches;
        switch (ID)
        {
            case 1:
                //Play Anim
#if UNITY_EDITOR_WIN
                if (Input.GetMouseButtonDown(0) && (Input.mousePosition.x <= m_fCentral && Input.mousePosition.x >= 0 && !Destroyer.m_bIsDead)
                     && m_bStop)
                {

#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8
                for (int i = 0; i < Input.touchCount; i++)
                {
                    if (myTouches[i].position.x <= m_fCentral && myTouches[i].position.x >= 0 
                    && isTouched == false && m_bStop && !Destroyer.m_bIsDead)
                    {
#endif
                    if (!HomeMenu.isMute)
                    {
                        GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
                    }
                    if (m_bIsLeft)
                    {
                        m_a_Controller.Play("Jump");
                        m_bStop = false;
                    }
                    if (!m_bIsLeft)
                    {
                        m_a_Controller.Play("Jump1");
                        m_bStop = false;
                    }
#if UNITY_EDITOR_WIN
                }
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8
                        isTouched = true;
                        break;
                    }
                }

                // touch release
                for (int i = 0; i < Input.touchCount; i++)
                {
                    if (TouchRelease(i) && myTouches[i].position.x <= m_fCentral && myTouches[i].position.x >= 0 
                    && isTouched == true && !Destroyer.m_bIsDead)
                    {
                        isTouched = false;
                        break;
                    }
                }
#endif
                ////Move Character
                if (!m_bStop && m_bIsLeft)
                {
                    if ((Sprite.transform.position.x > m_fDistant && Sprite.transform.position.x < -Sprite.transform.localScale.x))
                    {
                        transform.Translate(transform.position.x * -(m_fSpeed * 0.02f), 0, -0.1f);
                    }
                    else // standing mid
                    {
                        if (Sprite.transform.localScale.x > 0)
                        {
                            transform.position = new Vector3(m_flocalScaleAspect * -Sprite.transform.localScale.x, transform.position.y);
                            m_bStop = true;
                            m_bIsLeft = false;
                            EffectFallManager.m_bStop = true;
                            EffectFallManager.m_blIsLeft = false;
                        }
                        if (Sprite.transform.localScale.x < 0)
                        {
                            transform.position = new Vector3(m_flocalScaleAspect * Sprite.transform.localScale.x, transform.position.y);
                            m_bStop = true;
                            m_bIsLeft = false;
                            EffectFallManager.m_bStop = true;
                            EffectFallManager.m_blIsLeft = false;
                        }
                    }
                }
                if (!m_bStop && !m_bIsLeft)
                {
                    if ((Sprite.transform.position.x > (m_fDistant + Sprite.transform.localScale.x * -3f) && Sprite.transform.position.x < 0))
                    {
                        transform.Translate(transform.position.x * (m_fSpeed * 0.02f), 0, -0.1f);
                    }
                    else // standing left
                    {
                        transform.position = new Vector3((m_flocalScaleAspect - 0.195f) * (GameObject.Find("LeftTree").transform.position.x + Sprite.transform.localScale.x), transform.position.y);
                        m_bStop = true;
                        m_bIsLeft = true;
                        EffectFallManager.m_bStop = true;
                        EffectFallManager.m_blIsLeft = true;
                    }
                }
                break;

            case 2:
                //Play Anim
#if UNITY_EDITOR_WIN
                if (Input.GetMouseButtonDown(0) && (Input.mousePosition.x > m_fCentral) && m_bStop && !Destroyer.m_bIsDead)
                {

#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        if (myTouches[i].position.x > m_fCentral && isTouched == false && m_bStop && !Destroyer.m_bIsDead)
                        {
#endif
                    if (!HomeMenu.isMute)
                    {
                        GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
                    }
                    if (m_bIsLeft)
                    {
                        m_a_Controller.Play("Jump");
                        m_bStop = false;
                    }
                    if (!m_bIsLeft)
                    {
                        m_a_Controller.Play("Jump1");
                        m_bStop = false;
                    }
#if UNITY_EDITOR_WIN
                }
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8
                        isTouched = true;
                        break;
                    }
                }

                // touch release
                for (int i = 0; i < Input.touchCount; i++)
                {
                        if (TouchRelease(i) && myTouches[i].position.x > m_fCentral && isTouched == true && !Destroyer.m_bIsDead)
                        {
                            isTouched = false;
                            break;
                        }
                }
#endif
                ////Move Character
                if (!m_bStop && m_bIsLeft)
                {
                    if ((Sprite.transform.position.x > 0 && Sprite.transform.position.x < m_fDistant - Sprite.transform.localScale.x * 3.0f))
                    {
                        transform.Translate(transform.position.x * (m_fSpeed * 0.02f), 0, -0.1f);
                    }
                    else // stading right
                    {
                        transform.position = new Vector3((m_flocalScaleAspect - 0.195f) * (GameObject.Find("RightTree").transform.position.x + Sprite.transform.localScale.x), transform.position.y);
                        m_bStop = true;
                        m_bIsLeft = false;
                        EffectFallManager.m_bStop = true;
                        EffectFallManager.m_brIsLeft = false;
                    }
                } 
                if (!m_bStop && !m_bIsLeft)
                {
                    if ((Sprite.transform.position.x < m_fDistant && Sprite.transform.position.x > Sprite.transform.localScale.x * 2f))
                    {
                        transform.Translate(transform.position.x * -(m_fSpeed * 0.02f), 0, -0.1f);
                    }
                    else // stading mid
                    {
                        if (m_flocalScaleAspect * Sprite.transform.localScale.x > 0)
                        {
                            transform.position = new Vector3(m_flocalScaleAspect * Sprite.transform.localScale.x, transform.position.y);
                            m_bStop = true;
                            m_bIsLeft = true;
                            EffectFallManager.m_bStop = true;
                            EffectFallManager.m_brIsLeft = true;
                        }
                        if (m_flocalScaleAspect * Sprite.transform.localScale.x < 0)
                        {
                            transform.position = new Vector3(m_flocalScaleAspect * -Sprite.transform.localScale.x, transform.position.y);
                            m_bStop = true;
                            m_bIsLeft = true;
                            EffectFallManager.m_bStop = true;
                            EffectFallManager.m_brIsLeft = true;
                        }
                    }
                }
                break;

            default:
                break;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Juice" && other.tag != "Player")
        {
            if (!HomeMenu.isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSourcex2, 1F);
            }
            Destroy(other.gameObject);
            ScoreManager.x2Time = true;
            ShowX2.isTimetoX2 = true;
        }
        if (other.tag == "LifePoint" && other.tag != "Player")
        {
            if (!HomeMenu.isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSourceLifepoint, 1F);
            }
            Destroy(other.gameObject);
            if (ID == 1 && LifeManager.lHeal <= 95.0f)
            {
                LifeManager.lHeal += 10.0f;
            }
            if (ID == 2 && LifeManager.lHeal <= 95.0f)
            {
                LifeManager.rHeal += 10.0f;
            }
        }
        if (transform != null && other.tag != "Score_men" && other.tag != "Juice" && other.tag != "LifePoint" && other.tag != "Wall")
        {
            MoveManager.speedThorn = 0;
            Destroyer.m_bIsDead = true;
            if (!HomeMenu.isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSourceDie, 1F);
            }
            this.StartCoroutine(Despawn());
            
        }
    }

    private IEnumerator Despawn()
    {
        yield return new WaitForSeconds(1f);
        
        LoadGameoverScene();
    }

    private void LoadGameoverScene()
    {
        GameoverMenu.Losetime += 1;
        Application.LoadLevel("GameoverMenu");
        Time.timeScale = 1f;
    }

    private bool TouchRelease(int i)
    {
        bool b = false;
        b = Input.touches[i].phase == TouchPhase.Ended;
        if (b)
            return b;
        return b;
    }

}
