﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LifebarManager : MonoBehaviour
{
    public Transform lLifebar;
    public Transform rLifebar;

    private float MarginLeft;
    private float MarginTop;
    private float LocalScale;

    // Use this for initialization
    void Start()
    {
        MarginLeft = (Screen.width * 1.0f) / 4.0f;
        MarginTop = (Screen.height * 1.0f) * 0.95f;
        if(Screen.width < 480f)
            LocalScale = (Screen.height * 1.0f) / Screen.width;
        if(Screen.width >= 480f && Screen.width < 600f)
            LocalScale = (Screen.height * 1.5f) / (Screen.width);
        if (Screen.width >= 600f && Screen.width < 800f)
            LocalScale = (Screen.height * 2f) / (Screen.width);
        if (Screen.width >= 800f && Screen.width < 1080)
            LocalScale = (Screen.height * 3.0f) / (Screen.width);
        if (Screen.width >= 1080f)
            LocalScale = (Screen.height * 3.5f) / (Screen.width);
    }

    // Update is called once per frame
    void Update()
    {
        lLifebar.position = new Vector3(MarginLeft, MarginTop);
        rLifebar.position = new Vector3(3f * MarginLeft, MarginTop);

//#if UNITY_EDITOR_WIN
        lLifebar.localScale = new Vector3(LocalScale, LocalScale);
        rLifebar.localScale = new Vector3(LocalScale, LocalScale);
//#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8
//#endif
    }
}
