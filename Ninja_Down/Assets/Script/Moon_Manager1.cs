﻿using UnityEngine;
using System.Collections;

public class Moon_Manager1 : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        SpriteRenderer Sprite = GetComponent<SpriteRenderer>();
        Sprite m = Resources.Load("Sprites/Moon", typeof(Sprite)) as Sprite;
        Sprite.sprite = m;
        Sprite.transform.position = new Vector3((Screen.width * 1.0f / Screen.height) + m.bounds.size.x / 10f, ((Screen.width * 1.0f / Screen.height) * 0.75f) + m.bounds.size.y / 3.5f, -5f);
        Sprite.transform.localScale = new Vector3(m.bounds.size.x / 20.0f, m.bounds.size.y / 20.0f);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
