﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int Score;
    public static int Best;
    public static bool x2Time;
    public AudioClip auSource;

    void Start()
    {
        x2Time = false;
        Score = 0;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Thorn")
        {
            if (!HomeMenu.isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
            }
            if (x2Time)
            {
                Score += 2;
            }
            else
                Score += 1;
        }
    }
    void Update()
    {
        if (x2Time)
        {
            this.StartCoroutine(TimetoX2());
        }
    }
    private IEnumerator TimetoX2()
    {
        yield return new WaitForSeconds(5.0f);
        x2Time = false;
        ShowX2.isTimetoX2 = false;
    }
}
