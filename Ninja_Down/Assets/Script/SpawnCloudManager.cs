﻿using UnityEngine;
using System.Collections;

public class SpawnCloudManager : MonoBehaviour
{
    public Transform[] CloudPrefabs;
    private Transform CloudPrefab;
    private float spawnTime = 7.5f;		        // The amount of time between each spawn.
    private float spawnDelay = 0f;		        // The amount of time before spawning starts.

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("SpawnCloud", spawnDelay, Random.Range(spawnTime, spawnTime + 2.5f));
    }

    private void SpawnCloud()
    {
        this.transform.position = new Vector3(this.transform.position.x, Random.Range(-2.0f, 2.0f), -2.25f);
        CloudPrefab = CloudPrefabs[Random.Range(0, CloudPrefabs.Length)];
        Instantiate(CloudPrefab, transform.position, transform.rotation);
    }
}
