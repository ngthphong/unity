﻿using UnityEngine;
using System.Collections;

public class Spawnmanager : MonoBehaviour
{
    public int ID;
    public Transform[] ThornPrefabs;
    private Transform ThornPrefab;
    public static float spawnTime;		        // The amount of time between each spawn.
    private Sprite t;
    // Use this for initialization
    void Start()
    {
        spawnTime = 3f;
        t = Resources.Load("Sprites/gai", typeof(Sprite)) as Sprite;
        this.StartCoroutine(SpawnThorn());
    }

    private IEnumerator SpawnThorn()
    {
        do
        {
            int index = Random.Range(0, ThornPrefabs.Length);
            Vector3 spawnPos = new Vector3(0f, (Screen.height / Screen.width * 1.0f) * -5.0f, 0f);
            if (ID == 0)
            {
                //LeftSpawn
                switch (index)
                {
                    case 0: // Left
                        spawnPos.x = (Screen.width * 1.0f / Screen.height) / -t.bounds.size.x * 4.25f;
                        spawnPos.z = -1;
                        break;
                    case 1: // Right
                        spawnPos.x = -t.bounds.size.x / 5f;
                        spawnPos.z = -1;
                        break;
                    default:
                        break;
                }
                this.transform.position = spawnPos;
                ThornPrefab = ThornPrefabs[index];
                Instantiate(ThornPrefab, transform.position, transform.rotation);
            }
            //RightSpawn
            if (ID == 1)
            {
                switch (index)
                {
                    case 0: // Left
                        spawnPos.x = t.bounds.size.x / 4f;
                        spawnPos.z = -1;
                        break;
                    case 1: // Right
                        spawnPos.x = (Screen.width * 1.0f / Screen.height) / t.bounds.size.x * 4.25f;
                        spawnPos.z = -1;
                        break;
                    default:
                        break;
                }
                this.transform.position = spawnPos;
                ThornPrefab = ThornPrefabs[index];
                Instantiate(ThornPrefab, transform.position, transform.rotation);
            }
            yield return new WaitForSeconds(spawnTime + Random.Range(0f, 0.5f));
        }
        while (true);
    }
}
