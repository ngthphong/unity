﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour
{
    public static bool m_bIsDead;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Thorn" && other.tag != "Player")
        {
            Destroy(other.gameObject);
        }
    }

}
