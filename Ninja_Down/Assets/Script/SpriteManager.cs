﻿using UnityEngine;
using System.Collections;

public class SpriteManager : MonoBehaviour
{
    private SpriteRenderer Sprite;

    // Use this for initialization
    void Start()
    {
        if (HomeMenu.GAprefs != null)
        {
            if (GameStateManager.GameState == "Normal")
            {
                HomeMenu.GAprefs.LogScreen("Normal Mode");
                //Builder Hit with all App View parameters (all parameters required):
                HomeMenu.GAprefs.LogScreen(new AppViewHitBuilder()
                    .SetScreenName("Normal Mode"));
            }
            if (GameStateManager.GameState == "Survival")
            {
                HomeMenu.GAprefs.LogScreen("Survival Mode");
                //Builder Hit with all App View parameters (all parameters required):
                HomeMenu.GAprefs.LogScreen(new AppViewHitBuilder()
                    .SetScreenName("Survival Mode"));
            }
        }
    }
    void Awake()
    {
        Sprite = GetComponent<SpriteRenderer>();
        Sprite.transform.localScale = 10 * (new Vector3((Screen.width * 1.0f / Screen.height) / Sprite.bounds.size.x, 1.0f / Sprite.bounds.size.y, 1));
		//move func to camera script
        Screen.orientation = ScreenOrientation.Portrait;
    }
}
