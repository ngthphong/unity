﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour
{
    private SpriteRenderer Sprite;

    // Use this for initialization
    void Start()
    {
        Sprite = GetComponent<SpriteRenderer>();
        Sprite.transform.position = new Vector3(0f, (Screen.width * 1.0f / Screen.height) * 1.5f);
        this.StartCoroutine(LoadDone());
    }

    private IEnumerator LoadDone()
    {
        yield return new WaitForSeconds(3f);

        Application.LoadLevel("HomeMenu");
    }
}
