﻿using UnityEngine;
using System.Collections;

public class JuiceSpawner : MonoBehaviour
{
    public Transform[] JuicePrefabs;
    private Transform JuicePrefab;
    private float m_fspawnTime = 5f;		        // The amount of time between each spawn.
    private float m_fspawnDelay = 5f;		        // The amount of time before spawning starts.
    private float m_fleftMiddle;
    private float m_frightMiddle;
    private Sprite t;

    // Use this for initialization
    void Start()
    {
        t = Resources.Load("Sprites/ScoreandLife/trai dau", typeof(Sprite)) as Sprite;
        if (JuicePrefabs[0].gameObject.tag == "LifePoint")
        {
            InvokeRepeating("SpawnJuice", m_fspawnDelay, 1.5f);
        }
        else
        {
            InvokeRepeating("SpawnJuice", m_fspawnDelay, m_fspawnTime + Random.Range(5f, 10f));
        }
        m_fleftMiddle = (Screen.width * 1.0f / Screen.height) / -t.bounds.size.x * 2f; //left middle
        m_frightMiddle = (Screen.width * 1.0f / Screen.height) / t.bounds.size.x * 2.1f; //right middle
    }

    private void SpawnJuice()
    {
        int index = Random.Range(0, JuicePrefabs.Length);
        Vector3 spawnPos = new Vector3(0f, (Screen.height / Screen.width * 1.0f) * -5.0f, -1f);
        JuicePrefab = JuicePrefabs[index];
        int rand = Random.Range(0, 2); // random from 0 to 1
        if (rand == 0)
        {
            spawnPos.x = m_fleftMiddle;
        }
        if (rand == 1)
        {
            spawnPos.x = m_frightMiddle;
        }
        this.transform.position = spawnPos;
        Instantiate(JuicePrefab, transform.position, transform.rotation);
    }
}
