﻿using UnityEngine;
using System.Collections;

public class MoveManager : MonoBehaviour
{
    public static float speedThorn;

    void Start()
    {
        speedThorn = 6f;
        InvokeRepeating("IncreasSpeed", 1f, 20f);
    }
    private void IncreasSpeed()
    {
        if (speedThorn <= 11f)
        {
            speedThorn += 0.5f;
            if (Spawnmanager.spawnTime == 1f)
            {
                Spawnmanager.spawnTime -= 0.5f;
                speedThorn += 1f;
            }
            if (Spawnmanager.spawnTime > 1f)
            {
                Spawnmanager.spawnTime -= 0.5f;
            }
            

        }
    }
}
