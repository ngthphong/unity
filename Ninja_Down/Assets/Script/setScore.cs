﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class setScore : MonoBehaviour
{
    public Text Score;
    private int FSize = 25;
    private float MarginLeft;
    private float MarginTop;
    private float LocalScale;

    void Start()
    {
        // Set up the reference.
        MarginLeft = (Screen.width * 1.0f) * 0.75f;
        MarginTop = (Screen.height * 1.0f) * 0.915f;
        if (Screen.width < 400f)
            LocalScale = 1f;
        if (Screen.width > 480f && Screen.width < 600f)
            LocalScale = 1.5f;
        if (Screen.width < 800f)
            LocalScale = 2f;
        if (Screen.width >= 800f)
            LocalScale = 3.0f;
    }

    void Update()
    {
        Score.text = "Score: " + ScoreManager.Score;

#if UNITY_EDITOR_WIN
        Score.fontSize = FSize;
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8
        Score.fontSize = FSize;
        transform.localScale = new Vector3(LocalScale, LocalScale);
#endif
        Score.transform.position = new Vector3(MarginLeft, MarginTop * 0.99f);
    }
}
