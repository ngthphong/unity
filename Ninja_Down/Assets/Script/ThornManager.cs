﻿using UnityEngine;
using System.Collections;

public class ThornManager : MonoBehaviour
{
    void Start()
    {
        this.StartCoroutine(Despawner());
    }
    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, transform.localScale.y * MoveManager.speedThorn);
    }
    private IEnumerator Despawner()
    {
        yield return new WaitForSeconds(20f);
        if(transform.gameObject != null)
            Destroy(transform.gameObject);
    }
}
