﻿using UnityEngine;
using System.Collections;

public class ShowX2 : MonoBehaviour
{
    public static bool isTimetoX2;
    private float m_fleftMiddle;
    private Vector3 spawnPos;
    private Sprite t;
    // Use this for initialization
    void Start()
    {
        t = Resources.Load("Sprites/ScoreandLife/trai dau", typeof(Sprite)) as Sprite;
        isTimetoX2 = false;
        SpawnJuice();
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimetoX2)
        {
            spawnPos.z = -1f;
        }
        else
        {
            spawnPos.z = -10f;
        }
        this.transform.position = spawnPos;
    }
    private void SpawnJuice()
    {
        spawnPos = new Vector3(0f, 4.25f, -1f);
        m_fleftMiddle = (Screen.width * 1.0f / Screen.height) / -t.bounds.size.x * 2f; //left middle

        spawnPos.x = m_fleftMiddle;
        this.transform.position = spawnPos;
    }
}
