﻿using UnityEngine;
using System.Collections;

public class CloudManager : MonoBehaviour
{
    private float m_fSpeed = 0.5f;
    private float m_fScaletime;                   // set it if you want a bigger or smaller cloud
    private float m_fLifetime = 40f;             // life's time of the cloud

    // Use this for initialization
    void Start()
    {
        {
            transform.localScale += new Vector3(Random.Range(0, m_fScaletime), Random.Range(0, m_fScaletime));
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * (m_fSpeed * Time.deltaTime));
        this.StartCoroutine(DespawnCloud());
    }

    private IEnumerator DespawnCloud()
    {
        yield return new WaitForSeconds(m_fLifetime);
        if (this.transform != null)
        {
            DestroyObject(this.transform.gameObject);
        }
    }
}
