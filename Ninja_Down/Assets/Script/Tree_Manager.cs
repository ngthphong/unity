﻿using UnityEngine;
using System.Collections;

public class Tree_Manager : SpriteManager
{
    public int ID;
    private float Left;
    private float Right;
    // Use this for initialization
    void Awake()
    {
        SpriteRenderer Sprite = GetComponent<SpriteRenderer>();
        Sprite t = Resources.Load("Sprites/Tree", typeof(Sprite)) as Sprite;
        Left = (Screen.width * 1.0f / Screen.height) / -t.bounds.size.x * 2.4f;
        Right = (Screen.width * 1.0f / Screen.height) / t.bounds.size.x * 2.4f;
        Sprite.sprite = t;
        if (ID == 0) // left
        {
            Sprite.transform.position = new Vector3(Left, 0f, -0.1f);
        }
        if (ID == 1) // left
        {
            Sprite.transform.position = new Vector3(0f, 0f, -0.1f);
        }
        if (ID == 2) // right
        {
            Sprite.transform.position = new Vector3(Right, 0f, -0.1f);
        }

        Sprite.transform.localScale = new Vector3((Screen.width * 1.0f / Screen.height) / t.bounds.size.x / 2.0f, 10.0f / Sprite.bounds.size.y, 1.0f);
#if UNITY_EDITOR_WIN

#endif
    }
}
