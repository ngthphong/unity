﻿using UnityEngine;
using System.Collections;

public class EffectFallManager : MonoBehaviour
{
    public static bool m_bStop;
    public static bool m_blIsLeft;
    public static bool m_brIsLeft;
    public int ID;

    // Use this for initialization
    void Start()
    {
        m_bStop = true;
        if (ID == 1)
        {
            m_blIsLeft = true;
        }
        if (ID == 2)
        {
            m_brIsLeft = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_bStop && !m_blIsLeft && ID == 1)
        {
            transform.GetComponent<Animator>().Play("RightFall");
        }
        if (m_bStop && !m_brIsLeft && ID == 2)
        {
            transform.GetComponent<Animator>().Play("RightFall");
        }
        if (m_bStop && m_blIsLeft && ID == 1)
        {
            transform.GetComponent<Animator>().Play("LeftFall");
        }
        if (m_bStop && m_brIsLeft && ID == 2)
        {
            transform.GetComponent<Animator>().Play("LeftFall");
        }
    }
}
