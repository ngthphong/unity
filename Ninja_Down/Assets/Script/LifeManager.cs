﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LifeManager : MonoBehaviour
{
    public Scrollbar lLifebar;
    public static float lHeal;
    public Scrollbar rLifebar;
    public static float rHeal;
    private float speed;
    // Use this for initialization
    void Start()
    {
        lHeal = 100f;
        rHeal = 100f;
        speed = 2.0f;
        InvokeRepeating("IncreasSpeed", 1f, 10f);
    }

    // Update is called once per frame
    private void Update()
    {
        //left
        if (lHeal >= 0f)
        {
            lHeal -= speed * Time.deltaTime;
            lLifebar.size = lHeal / 100f;
        }

        //right
        if (rHeal >= 0f)
        {
            rHeal -= speed * Time.deltaTime;
            rLifebar.size = rHeal / 100f;
        }
        if (lHeal <= 5f || rHeal <= 5f)
        {
            Application.LoadLevel("GameoverMenu");
        }
    }
    private void IncreasSpeed()
    {
        if (speed <= 10.0f && lHeal > 0f && rHeal > 0f)
        {
            speed += 0.5f;
        }
    }
}
