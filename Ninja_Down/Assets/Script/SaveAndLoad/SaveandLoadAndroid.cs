﻿using UnityEngine;
using System.Collections;
#if !(UNITY_METRO || UNITY_WP8)
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveandLoadAndroid : MonoBehaviour
{
    //public static List<Game> savedGames = new List<Game>();
    public static int Score;
    public static bool Mute;

    //it's static so we can call it from anywhere
    public static void Save()
    {
        //SaveLoad.savedGames.Add(Game.current);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd"); //you can call it anything you want
        bf.Serialize(file, Score);
        file.Close();
        file = File.Create(Application.persistentDataPath + "/Setting.gd"); //you can call it anything you want
        bf.Serialize(file, Mute);
        file.Close();
    }

    public static void Load()
    {
        BinaryFormatter bf;
        
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            Score = (int)bf.Deserialize(file);
            file.Close();
        }
        if (File.Exists(Application.persistentDataPath + "/Setting.gd"))
        {
            bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/Setting.gd", FileMode.Open);
            Mute = (bool)bf.Deserialize(file);
            file.Close();
        }
        if (!File.Exists(Application.persistentDataPath + "/Setting.gd"))
        {
            Mute = false;
        }
    }
}
#endif