﻿using UnityEngine;
using System.Collections;
#if !(UNITY_METRO || UNITY_WP8)
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
[System.Serializable]

class VariablesforSaving
{

    public int Score;

    public VariablesforSaving()
    {
        Score = 0;
    }

    //Deserialization constructor

    public VariablesforSaving(SerializationInfo info, StreamingContext ctxt)
    {
        //Get the values from info and assign them to the appropriate properties

        Score = (int)info.GetValue("Score", typeof(int));
    }

    //Serialization function.

    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        //You can use any name/value pair, as long as you read them with same names

        info.AddValue("Score", Score);
    }

}


public class SaveandLoad : MonoBehaviour
{
    public static int Score;

    public void Save()
    {
        VariablesforSaving MyVariables = new VariablesforSaving();

        MyVariables.Score = Score;

        Stream stream = File.Open("Score.sav", FileMode.Create);

        BinaryFormatter bformatter = new BinaryFormatter();

        bformatter.Serialize(stream, MyVariables);

        stream.Close();
    }

    public void Load()
    {
        VariablesforSaving MyVariables = new VariablesforSaving();

        Stream stream = File.Open("Score.sav", FileMode.Open);

        BinaryFormatter bformatter = new BinaryFormatter();

        MyVariables = (VariablesforSaving)bformatter.Deserialize(stream);

        Score = MyVariables.Score;
        stream.Close();
    }

    public void Update()
    {
        if (Input.GetKeyDown("s"))
            Save();

        if (Input.GetKeyDown("l"))
            Load();
    }
}
#endif