﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class setScoreGameOver : MonoBehaviour
{
    public Text titScore;
    public Text Score;
    public Text titBest;
    public Text Best;

    private int FSize = 30;
    private float m_fMarginLeft;
    private float m_fMarginTop;
    private float m_fWidth;
    private float m_fHeight;
    private float m_fLocalScale;

    // Use this for initialization
    void Start()
    {
        m_fWidth = Mathf.Min(Screen.width, Screen.height);
        m_fHeight= Mathf.Max(Screen.width, Screen.height);
        m_fMarginTop = m_fHeight / 1.525f;
        m_fMarginLeft = m_fWidth / 2f;
        if (Screen.width < 480)
        {
            m_fLocalScale = 2.0f;
        }
        if (Screen.width >= 480 && Screen.width < 720)
        {
            m_fLocalScale = 3.0f;
        }
        if (Screen.width >= 720 && Screen.width < 854)
        {
            m_fLocalScale = 4.0f;
        }
        if (Screen.width >= 854 && Screen.width < 1080)
        {
            m_fLocalScale = 5.0f;
        }
        if (Screen.width >= 1080)
        {
            m_fLocalScale = 6.0f;
        }
    }

    void Update()
    {

        // Score
        Score.text = ScoreManager.Score.ToString();
        Best.text = ScoreManager.Best.ToString();

        titScore.fontSize = FSize;
        Score.fontSize = FSize;
        titBest.fontSize = FSize;
        Best.fontSize = FSize;

        transform.localScale = new Vector3(m_fLocalScale, m_fLocalScale);

#if UNITY_EDITOR_WIN
        titScore.transform.position = new Vector3(m_fMarginLeft, m_fMarginTop);
        Score.transform.position = new Vector3(m_fMarginLeft, m_fMarginTop - Score.fontSize);

        titBest.transform.position = new Vector3(m_fMarginLeft, m_fMarginTop - titBest.fontSize * 2.0f);
        Best.transform.position = new Vector3(m_fMarginLeft, m_fMarginTop - Best.fontSize * 3.0f);

#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8

        titScore.transform.position = new Vector3(m_fMarginLeft, m_fMarginTop);
        Score.transform.position = new Vector3(m_fMarginLeft, m_fMarginTop - Score.fontSize * (m_fLocalScale / 2.0f));

        titBest.transform.position = new Vector3(m_fMarginLeft, m_fMarginTop - titBest.fontSize * (m_fLocalScale / 2.0f) * 2.5f);
        Best.transform.position = new Vector3(m_fMarginLeft, m_fMarginTop - Best.fontSize * (m_fLocalScale / 2.0f) * 3.75f);
#endif
    }
}
