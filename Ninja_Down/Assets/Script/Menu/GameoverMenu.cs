﻿using UnityEngine;
#if UNITY_EDITOR_WIN
#elif UNITY_ANDROID
using StartApp;
#endif
using System.Collections;

public class GameoverMenu : MonoBehaviour
{
    public static int Losetime;
    private GUISkin m_gsButtonRetry;
    private GUISkin m_gsButtonHome;
    private GUISkin m_gsButtonRank;
    private GUISkin m_gsButtonRate;
    private GUISkin m_gsButtonMore;
    public AudioClip auSource;

    // Use this for initialization
    void Start()
    {
        if (HomeMenu.GAprefs != null)
        {
            HomeMenu.GAprefs.LogScreen("GameOver");
            //Builder Hit with all App View parameters (all parameters required):
            HomeMenu.GAprefs.LogScreen(new AppViewHitBuilder()
                .SetScreenName("GameOver"));
        }

#if UNITY_ANDROID && !UNITY_EDITOR_WIN
        if(Losetime >= 3)
        {
            Losetime = 0;
            StartAppWrapper.showAd();
            StartAppWrapper.loadAd();
        }
#endif
        Screen.orientation = ScreenOrientation.Portrait;
        // Set for first play
        if (PlayerPrefs.GetInt("BestScore") == 0)
        {
            ScoreManager.Best = ScoreManager.Score;
            PlayerPrefs.SetInt("BestScore", ScoreManager.Best);
        }

        // Load Score
        if (ScoreManager.Best < PlayerPrefs.GetInt("BestScore"))
        {
            PlayerPrefs.SetInt("BestScore", ScoreManager.Best);
        }
        if (ScoreManager.Score > ScoreManager.Best)
        {
            ScoreManager.Best = ScoreManager.Score;
            PlayerPrefs.SetInt("BestScore", ScoreManager.Best);
        }
        PlayerPrefs.Save();
#if UNITY_EDITOR_WIN
        #if !(UNITY_METRO || UNITY_WP8)
        if (ScoreManager.Best > SaveandLoad.Score)
        {
            SaveandLoad.Score = ScoreManager.Best;
            SaveandLoad sal = new SaveandLoad();
            sal.Save();
        }
        #endif
#elif UNITY_ANDROID
        if (ScoreManager.Best > SaveandLoadAndroid.Score)
        {
            SaveandLoadAndroid.Score = ScoreManager.Best;
            SaveandLoadAndroid.Save();
        }
#endif

        // Load a skin for the buttons
        m_gsButtonRetry = Resources.Load("Menu/ButtonRetry") as GUISkin;
        m_gsButtonHome = Resources.Load("Menu/ButtonHome") as GUISkin;
        m_gsButtonRank = Resources.Load("Menu/ButtonRank") as GUISkin;
        m_gsButtonRate = Resources.Load("Menu/ButtonRate") as GUISkin;
        m_gsButtonMore = Resources.Load("Menu/ButtonMore") as GUISkin;
    }

    // Update is called once per frame
    void OnGUI()
    {
        float buttonWidth = Screen.width / 4f;
        float buttonHeight = Screen.width / 4f;
        float columd = Screen.width / 4.0f;
        float secrow = (2.0f * Screen.height / 3.0f) - (buttonHeight / 2.0f) + buttonHeight;

        GUI.skin = m_gsButtonRetry;
        if (GUI.Button(new Rect(2.0f * columd - (buttonWidth / 2.0f) + 3.0f, (2.0f * Screen.height / 3.0f) - (buttonHeight / 2.0f), buttonWidth, buttonHeight), ""))
        {
            //Retry
            if (!HomeMenu.isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
            }
            Application.LoadLevel(GameStateManager.GameState);
        }

        ////Home
        GUI.skin = m_gsButtonHome;
        if (GUI.Button(new Rect(columd - (buttonWidth / 2.0f) + 4f, secrow, buttonWidth, buttonHeight), ""))
        {
            if (!HomeMenu.isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
                Application.LoadLevel("HomeMenu");
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
                StartAppWrapper.showAd();
                StartAppWrapper.loadAd();
#endif
            }
        }
        ////Rank

        //GUI.skin = m_gsButtonRank;
        //if (GUI.Button(new Rect(columd + 4f, secrow, buttonWidth, buttonHeight), ""))
        //{
        //    if (!HomeMenu.isMute)
        //    {
        //        GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
        //    }
        //}

        //Rate
        GUI.skin = m_gsButtonRate;
        //if (GUI.Button(new Rect(columd + buttonWidth + 4f, secrow, buttonWidth, buttonHeight), ""))
        if (GUI.Button(new Rect(columd + (buttonWidth / 2.0f) + 4f, secrow, buttonWidth, buttonHeight), ""))
        {
            if (!HomeMenu.isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
                Application.OpenURL("https://play.google.com/store/apps/details?id=com.NiwiN.AmazingDown");
#endif
            }
        }

        //More
        GUI.skin = m_gsButtonMore;
        //if (GUI.Button(new Rect(columd + 2.0f * buttonWidth + 4f, secrow, buttonWidth, buttonHeight), ""))
        if (GUI.Button(new Rect(columd + (buttonWidth * 1.5f) + 4f, secrow, buttonWidth, buttonHeight), ""))
        {
            if (!HomeMenu.isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
                Application.OpenURL("https://play.google.com/store/apps/developer?id=Team3TH");
#endif
            }
        }
    }
}
