﻿using UnityEngine;
#if UNITY_ANDROID && !UNITY_EDITOR
using StartApp;
#endif
using System.Collections;

public class HomeMenu : MonoBehaviour
{
    public static bool isMute;
    private GUISkin m_gsButtonCup;
    private GUISkin m_gsButtonRank;
    private GUISkin m_gsButtonRate;
    private GUISkin m_gsButtonMore;
    private GUISkin m_gsButtonSound;
    private GUISkin m_gsButtonMute;
    private GUISkin m_gsButtonNormal;
    private GUISkin m_gsButtonHell;
    private GUISkin m_gsButtonSurvival;
    public AudioClip auSource;

    // google analytics
    public GoogleAnalyticsV3 googleAnalytics;
    public static GoogleAnalyticsV3 GAprefs;

    // Use this for initialization
    void Start()
    {
        googleAnalytics.LogScreen("Main Menu");
        //Builder Hit with all App View parameters (all parameters required):
        googleAnalytics.LogScreen(new AppViewHitBuilder()
            .SetScreenName("Home Menu"));

        GAprefs = googleAnalytics;

        GameoverMenu.Losetime = 0;
#if UNITY_ANDROID && !UNITY_EDITOR
        StartAppWrapper.addBanner(
        StartAppWrapper.BannerType.AUTOMATIC,
        StartAppWrapper.BannerPosition.BOTTOM);
#endif
        Screen.orientation = ScreenOrientation.Portrait;
        m_gsButtonNormal = Resources.Load("Menu/ButtonNormal") as GUISkin;
        m_gsButtonHell = Resources.Load("Menu/ButtonHell") as GUISkin;
        m_gsButtonSurvival = Resources.Load("Menu/ButtonSurvival") as GUISkin;
        m_gsButtonCup = Resources.Load("Menu/ButtonCup") as GUISkin;
        m_gsButtonRank = Resources.Load("Menu/ButtonRank") as GUISkin;
        m_gsButtonMore = Resources.Load("Menu/ButtonMore") as GUISkin;
        m_gsButtonSound = Resources.Load("Menu/ButtonSound") as GUISkin;
        m_gsButtonMute = Resources.Load("Menu/ButtonMute") as GUISkin;
        m_gsButtonRate = Resources.Load("Menu/ButtonRate") as GUISkin;
#if UNITY_EDITOR_WIN
        #if !(UNITY_METRO || UNITY_WP8)
        if (System.IO.File.Exists("Score.sav"))
        {
            SaveandLoad sal = new SaveandLoad();
            sal.Load();
            ScoreManager.Best = SaveandLoad.Score;
        }
        #endif
#elif UNITY_ANDROID
        SaveandLoadAndroid.Load();
        ScoreManager.Best = SaveandLoadAndroid.Score;
        isMute = SaveandLoadAndroid.Mute;
#endif
    }

    // Update is called once per frame
    void OnGUI()
    {
        float buttonWidth = Screen.width / 4.0f;
        float buttonHeight = Screen.width / 4.0f;
        float columd = Screen.width / 4.0f;
        float buttonPlayWidth = Screen.width / 2.0f;
        float buttonPlayHeight = Screen.width / 4.0f;
        float secrow = (2.0f * Screen.height / 3.5f) - (buttonHeight / 2.0f) / 3.0f;
//#if UNITY_EDITOR_WIN
        GUI.skin = m_gsButtonNormal;
        if (GUI.Button(new Rect(Screen.width - 1.475f * buttonPlayWidth, 1.5f * buttonPlayHeight, buttonPlayWidth, buttonPlayHeight), ""))
        {
            //Normal
            if (!isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
            }
            GameStateManager.GameState = "Normal";
            if(!Application.isLoadingLevel)
                Application.LoadLevel("Normal");
        }
        //GUI.skin = m_gsButtonHell;
        GUI.skin = m_gsButtonSurvival;
        if (GUI.Button(new Rect(Screen.width - 1.475f * buttonPlayWidth, 2.5f * buttonPlayHeight, buttonPlayWidth, buttonPlayHeight), ""))
        {
            //Hard
            if (!isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
            }
            //GameStateManager.GameState = "Normal";
            //Application.LoadLevel("Normal");
            GameStateManager.GameState = "Survival";
            Application.LoadLevel("Survival");
        }
        //GUI.skin = m_gsButtonSurvival;
        //if (GUI.Button(new Rect(Screen.width - 1.475f * buttonPlayWidth, 3.5f * buttonPlayHeight, buttonPlayWidth, buttonPlayHeight), ""))
        //{
        //    //Hell
        //    if (!isMute)
        //    {
        //        GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
        //    }
        //    GameStateManager.GameState = "Survival";
        //    Application.LoadLevel("Survival");
        //}

        //Achivment
        //GUI.skin = m_gsButtonCup;
        //Rate
        GUI.skin = m_gsButtonRate;
        if (GUI.Button(new Rect(columd - (buttonWidth / 2.0f) + 4f, secrow, buttonWidth, buttonHeight), ""))
        {
            if (!isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
                Application.OpenURL("https://play.google.com/store/apps/details?id=com.NiwiN.AmazingDown");
#endif
            }
        }

        //Sound
        if (!isMute)
        {
            GUI.skin = m_gsButtonSound;
            if (GUI.Button(new Rect(columd + (buttonWidth / 2.0f) + 4f, secrow, buttonWidth, buttonHeight), ""))
            {
                if (!isMute)
                {
                    GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
                }
                isMute = true;
#if UNITY_ANDROID && !UNITY_EDITOR
                SaveandLoadAndroid.Mute = isMute;
                SaveandLoadAndroid.Save();
#endif
            }
        }
        if (isMute)
        {
            GUI.skin = m_gsButtonMute;
            if (GUI.Button(new Rect(columd + (buttonWidth / 2.0f) + 4f, secrow, buttonWidth, buttonHeight), ""))
            {
                if (!isMute)
                {
                    GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
                }
                isMute = false;
#if UNITY_ANDROID && !UNITY_EDITOR
                SaveandLoadAndroid.Mute = isMute;
                SaveandLoadAndroid.Save();
#endif
            }
        }

        GUI.skin = m_gsButtonMore;
        if (GUI.Button(new Rect(columd + 1.5f * buttonWidth + 4f, secrow, buttonWidth, buttonHeight), ""))
        {
            if (!isMute)
            {
                GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
#if UNITY_ANDROID && !UNITY_EDITOR_WIN
                Application.OpenURL("https://play.google.com/store/apps/developer?id=Team3TH");
#endif
            }
        }

        //Rank
        //GUI.skin = m_gsButtonRank;
        //if (GUI.Button(new Rect(columd + 4f, secrow, buttonWidth, buttonHeight), ""))
        //{
        //    if (!isMute)
        //    {
        //        GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
        //    }
        //}

        //Rate
        //GUI.skin = m_gsButtonRate;
        //if (GUI.Button(new Rect(columd + buttonWidth + 4f, secrow, buttonWidth, buttonHeight), ""))
        //{
        //    if (!isMute)
        //    {
        //        GetComponent<AudioSource>().PlayOneShot(auSource, 1F);
        //    }
        //}

        //More
        
    }
}
